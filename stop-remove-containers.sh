docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

docker volume rm $(docker volume ls -q)

rm -rf crypto-config
rm -rf channel-artifacts