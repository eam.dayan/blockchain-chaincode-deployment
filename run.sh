#!/bin/bash

export IMAGE_TAG=latest

echo $IMAGE_TAG
export PATH=$PATH:$PWD/bin
# if [[ -d "./crypto-config" ]]
# then
	echo "Step 1 :>>"
	echo "===================== Generating Artifacts ========================"
	cryptogen generate --config=./crypto-config.yaml

	echo "Step 2 :>>"
	echo "===================== Generating Genesis Block ========================"
	export FABRIC_CFG_PATH=$PWD
	configtxgen -profile Raft -channelID workspace-sys-channel -outputBlock ./channel-artifacts/genesis.block

	echo "Step 3 :>>"
	echo "===================== Create Channel Transaction Artifact ========================
	Note: This step and step 4 doesn’t make any actual blockchain transaction, instead generates transaction artifacts that will aid in performing transactions in step 6 and 7."
	export CHANNEL_NAME=workspace
	# FourOrgsChannel is the Channel name declared in config.yaml > Profile section
	configtxgen -profile FourOrgsChannel -outputCreateChannelTx ./channel-artifacts/workspace.tx -channelID $CHANNEL_NAME

	echo "Step 4 :>>"
	echo "===================== Create Anchor Peers Artifacts ========================"
	echo "For developer organization"

	configtxgen -profile FourOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org1MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org1MSP

	echo "For accounts organization"

	configtxgen -profile FourOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org2MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org2MSP

	echo "For hr organization"

	configtxgen -profile FourOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org3MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org3MSP

	echo "For marketing organization"

	configtxgen -profile FourOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org4MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org4MSP

	echo " <<<<<<<<<<<<< END CREATING ANCHOR PEER FOR EACH ORGANIZATIONs >>>>>>>>>>>>>>>" 

# fi

/////////////////////?/////////////////////?/////////////////////?/////////////////////?
echo "Step 5 :>>"
echo "===================== Starting Network ========================"
export COMPOSE_PROJECT_NAME=net
export IMAGE_TAG=latest
export SYS_CHANNEL=workspace-sys-channel

docker-compose -f docker-compose.yaml up -d
echo "Done ||||||||| Please check ./scripts for peer channel install"


sleep 4s
docker exec -it cli bash -c ". scripts/createChannel.sh"
sleep 4s

docker exec -it cli bash -c ". scripts/devPeer1.sh"
docker exec -it cli bash -c ". scripts/accountPeer1.sh"
docker exec -it cli bash -c ". scripts/hrPeer1.sh"
docker exec -it cli bash -c ". scripts/marketingPeer1.sh"

echo "============================== Starting Hyperledger Explorer =================================="
docker-compose -f explorer/docker-compose.yaml up -d

echo "============================== Starting Grafana =================================="
docker-compose -f test-network/moniter\ and\ logging/monitering/docker-compose.yml up -d

# echo "============================== Package Chaincode =================================="
# pushd ./chaincode || exit 1
# go mod init sacc.go
# go mod tidy
# GO111MODULE=on go mod vendor
# popd || exit 1



# export FABRIC_CFG_PATH=$PWD/config

# peer lifecycle chaincode package ./chaincode/sacc.tar.gz --lang golang --label "sacc_1" --path ./chaincode

echo "============================== Installing Chaincode =================================="

docker exec -it cli bash -c ". scripts/chaincode/installChaincodeDevPeer1.sh"
docker exec -it cli bash -c ". scripts/chaincode/installChaincodeAccountingPeer1.sh"
docker exec -it cli bash -c ". scripts/chaincode/installChaincodeHrPeer1.sh"
docker exec -it cli bash -c ". scripts/chaincode/installChaincodeMarketingPeer1.sh"

echo "============================== approving Chaincode =================================="

docker exec -it cli bash -c ". scripts/chaincode/approveChaincodeDevPeer1.sh"
docker exec -it cli bash -c ". scripts/chaincode/approveChaincodeAccountingPeer1.sh"
docker exec -it cli bash -c ". scripts/chaincode/approveChaincodeHrPeer1.sh"
docker exec -it cli bash -c ". scripts/chaincode/approveChaincodeMarketingPeer1.sh"

docker exec -it cli bash -c ". scripts/chaincode/commitChaincode.sh"

echo "============================== instantiate Chaincode =================================="

docker exec -it cli bash -c ". scripts/chaincode/initLedger.sh"