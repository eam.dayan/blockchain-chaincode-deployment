# hyperledger-fabric-network-from-scratch

## Starting the network

Run the two shellscripts below to:

- start the network
- create the channel
- join peers to the channel
- deploy chaincode
- initialize ledger with chaincode

``` bash
. stop-remove-containers.sh
. run.sh
```
