#!/bin/bash

echo "======= creating channel ========="

export CHANNEL_NAME=workspace

peer channel create -o orderer1.workspace:7050 \
	-c "$CHANNEL_NAME" -f ./channel-artifacts/"$CHANNEL_NAME".tx \
	--outputBlock ./"$CHANNEL_NAME".block \
	--tls \
	--cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem
ls