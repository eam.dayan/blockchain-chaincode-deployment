#!/bin/bash

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/users/Admin@developers.workspace/msp
export CHANNEL_NAME=workspace

export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_ADDRESS="peer1.developers.workspace:7051"
export ORDERER_ADDRESS="orderer1.workspace:7050"

export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem

### tmp
export COMPOSE_PROJECT_NAME=net
export IMAGE_TAG=latest
export SYS_CHANNEL=workspace-sys-channel 

export CHANNEL_NAME=workspace
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/users/Admin@developers.workspace/msp
export CORE_PEER_ADDRESS="peer1.developers.workspace:7051"
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt
###

export ROOT_CERT_FILE_PEER1_DEVELOPERS=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt
# ROOT_CERT_FILE_PEER1_ACCOUNTS=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/accounts.workspace/users/Admin@accounts.workspace/tls/ca.crt
# ROOT_CERT_FILE_PEER1_HR=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hr.workspace/users/Admin@hr.workspace/tls/ca.crt
# ROOT_CERT_FILE_PEER1_MARKETING=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/marketing.workspace/users/Admin@marketing.workspace/tls/ca.crt

CHAINCODE_NAME=sacc
VERSION=1
SEQUENCE=1

peer lifecycle chaincode checkcommitreadiness \
    --channelID "$CHANNEL_NAME" --name "$CHAINCODE_NAME" \
    --version "$VERSION" --sequence "$SEQUENCE" \
    --init-required

peer lifecycle chaincode commit -o "$ORDERER_ADDRESS" \
    --channelID "$CHANNEL_NAME" --name "$CHAINCODE_NAME" \
    --version "$VERSION" --sequence "$SEQUENCE" \
    --init-required \
    --peerAddresses peer1.developers.workspace:7051 \
    --tls \
    --cafile "$ORDERER_CA" \
    --tlsRootCertFiles "$ROOT_CERT_FILE_PEER1_DEVELOPERS" 
    # --tlsRootCertFiles "$ROOT_CERT_FILE_PEER1_ACCOUNTS" \
    # --tlsRootCertFiles "$ROOT_CERT_FILE_PEER1_HR" \
    # --tlsRootCertFiles "$ROOT_CERT_FILE_PEER1_MARKETING"

peer lifecycle chaincode querycommitted \
    --channelID "$CHANNEL_NAME" \
    --name "$CHAINCODE_NAME"


peer chaincode invoke -o "$ORDERER_ADDRESS" \
    --channelID "$CHANNEL_NAME" --name "$CHAINCODE_NAME" \
    --isInit \
    --peerAddresses peer1.developers.workspace:7051 \
    --tls \
    --cafile "$ORDERER_CA" \
    --tlsRootCertFiles "$ROOT_CERT_FILE_PEER1_DEVELOPERS" \
    -c '{"Args":["InitLedger"]}'