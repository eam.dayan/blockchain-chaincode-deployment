#!/bin/bash

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/marketing.workspace/users/Admin@marketing.workspace/msp
export CORE_PEER_ADDRESS=peer1.marketing.workspace:13051
export CORE_PEER_LOCALMSPID="Org4MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/marketing.workspace/peers/peer1.marketing.workspace/tls/ca.crt
export CHANNEL_NAME=workspace

export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem
export CHAINCODE_PATH=/opt/gopath/src/github.com/chaincode

pushd "$CHAINCODE_PATH" || exit 1
go mod init sacc.go
go mod tidy
GO111MODULE=on go mod vendor
popd || exit 1

# export FABRIC_CFG_PATH=$PWD/config

peer lifecycle chaincode package ./sacc.tar.gz --lang golang --label "sacc_1" --path "$CHAINCODE_PATH"

peer lifecycle chaincode install /opt/gopath/src/github.com/chaincode/sacc.tar.gz
