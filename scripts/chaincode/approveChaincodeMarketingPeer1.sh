#!/bin/bash

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/marketing.workspace/users/Admin@marketing.workspace/msp
export CORE_PEER_ADDRESS=peer1.marketing.workspace:13051
export CORE_PEER_LOCALMSPID="Org4MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/marketing.workspace/peers/peer1.marketing.workspace/tls/ca.crt
export CHANNEL_NAME=workspace

export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem


CHAINCODE_NAME=sacc
VERSION=1
SEQUENCE=1
ORDERER_ADDRESS="orderer1.workspace:7050"

peer lifecycle chaincode queryinstalled >&log.txt
cat log.txt
PACKAGE_ID=$(sed -n "/sacc_1/{s/^Package ID: //; s/, Label:.*$//;p;}" log.txt)


peer lifecycle chaincode approveformyorg -o "$ORDERER_ADDRESS" \
    --channelID "$CHANNEL_NAME" --name "$CHAINCODE_NAME" --version "$VERSION" \
    --tls --cafile "$ORDERER_CA" \
    --init-required --package-id "$PACKAGE_ID" --sequence "$SEQUENCE"
