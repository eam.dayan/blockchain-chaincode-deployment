#!/bin/bash

export CHANNEL_NAME=workspace
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/users/Admin@developers.workspace/msp
export CORE_PEER_ADDRESS=peer1.developers.workspace:7051
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt

export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem

export CHAINCODE_PATH=/opt/gopath/src/github.com/chaincode

CHAINCODE_NAME=sacc
VERSION=1
SEQUENCE=1

peer lifecycle chaincode queryinstalled >&log.txt
cat log.txt
PACKAGE_ID=$(sed -n "/sacc_1/{s/^Package ID: //; s/, Label:.*$//;p;}" log.txt)

echo "============================= package id"
echo $PACKAGE_ID

peer lifecycle chaincode approveformyorg \
    --channelID "$CHANNEL_NAME" --name "$CHAINCODE_NAME" --version "$VERSION" \
    --tls --cafile "$ORDERER_CA" \
    --init-required --package-id "$PACKAGE_ID" --sequence "$SEQUENCE"
