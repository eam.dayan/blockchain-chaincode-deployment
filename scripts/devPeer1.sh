#!/bin/bash
echo "===================== Starting Network ========================"

export COMPOSE_PROJECT_NAME=net
export IMAGE_TAG=latest
export SYS_CHANNEL=workspace-sys-channel 

export CHANNEL_NAME=workspace
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/users/Admin@developers.workspace/msp
export CORE_PEER_ADDRESS="peer1.developers.workspace:7051"
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt


# peer channel create -o orderer1.workspace:7050 \
# 	-c "$CHANNEL_NAME" -f ./channel-artifacts/"$CHANNEL_NAME".tx \
# 	--outputBlock ./"$CHANNEL_NAME".block \
# 	--tls \
# 	--cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem


peer channel join -b ./workspace.block


peer channel update \
	-o orderer1.workspace:7050 \
	-c "$CHANNEL_NAME" \
	-f ./channel-artifacts/Org1MSPanchors.tx \
	--tls \
	--cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem